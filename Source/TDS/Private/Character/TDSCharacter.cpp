// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/TDSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	/*CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(
		TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	*/

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	CameraHeightInterpolate = CameraBoom->TargetArmLength;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(this, CursorMaterial, CursorSize, FVector(ForceInitToZero));
	}
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			const FVector CursorFV = TraceHitResult.ImpactNormal;
			const FRotator CursorR = CursorFV.Rotation();
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	CameraTick(DeltaSeconds);
	MovementTick(DeltaSeconds);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Axis bindings
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::OnMoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::OnMoveRight);
	PlayerInputComponent->BindAxis(TEXT("CameraZoom"), this, &ATDSCharacter::OnCameraZoomChange);

	//Action bindings
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &ATDSCharacter::OnWalkPressed);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &ATDSCharacter::OnWalkReleased);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATDSCharacter::OnSprintPressed);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATDSCharacter::OnSprintReleased);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &ATDSCharacter::OnAimPressed);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &ATDSCharacter::OnAimReleased);
	PlayerInputComponent->BindAction(TEXT("F"), IE_Pressed, this, &ATDSCharacter::OnTestActionPressed);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ATDSCharacter::OnFirePressed);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ATDSCharacter::OnFireReleased);
	// PlayerInputComponent->BindAction(TEXT("F"), IE_Released, this, );
}

void ATDSCharacter::InitWeapon()
{
	if (InitWeaponClass)
	{
		const FVector SpawnLocation = FVector(0);
		const FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = GetInstigator();

		AWeaponDefault* MyWeapon = GetWorld()->SpawnActor<AWeaponDefault>(InitWeaponClass, SpawnLocation, SpawnRotation, SpawnParameters);
		if (MyWeapon)
		{
			MyWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, TEXT("WeaponSocketRightHand"));
			CurrentWeapon = MyWeapon;

			MyWeapon->UpdateStateWeapon(MovementState);
		}
	}
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	auto MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharEvent - CurrentWeapon - nullptr"))
	}
}


void ATDSCharacter::MovementTick(float DeltaTime)
{
	CharacterUpdate();
	AddMovementInput(FVector(1, 0, 0), AxisX);
	AddMovementInput(FVector(0, 1, 0), AxisY);

	auto MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult HitResult;
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel11, false, HitResult);
		const auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
		auto DesiredRotation = FRotator(0.0f, LookAtRotation.Yaw, 0.0f);
		SetActorRotation(FQuat(DesiredRotation));
	}
}

void ATDSCharacter::CameraTick(float DeltaTime)
{
	CameraBoom->TargetArmLength = FMath::FInterpTo(
		CameraBoom->TargetArmLength,
		CameraHeightInterpolate,
		DeltaTime,
		3.f);
}

void ATDSCharacter::CharacterUpdate()
{
	float ResultSpeed = 600.f;

	if (MovementState == EMovementState::Aim_State)
	{
		ResultSpeed = MovementInfo.AimSpeed;
	}
	else if (MovementState == EMovementState::Walk_State)
	{
		ResultSpeed = MovementInfo.WalkSpeed;
	}
	else if (MovementState == EMovementState::Sprint_State && IsMovingForward())
	{
		ResultSpeed = MovementInfo.SprintSpeed;
	}
	else if (MovementState == EMovementState::Run_State)
	{
		ResultSpeed = MovementInfo.RunSpeed;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
	GetCurrentWeapon()->UpdateStateWeapon(MovementState);
}

void ATDSCharacter::ChangeMovementState(const EMovementState NewMovementState)
{
	MovementState = NewMovementState;
}

bool ATDSCharacter::IsMovingForward() const
{
	auto ForwardVector = GetActorForwardVector();
	ForwardVector.Normalize();
	auto Velocity = GetVelocity();
	Velocity.Normalize();
	const auto DotProduct = FVector::DotProduct(ForwardVector, Velocity);
	return FMath::IsNearlyEqual(DotProduct, 1.f, 0.05f);
}

/** Input event handlers area */

/** Handler for MoveForward axis */
void ATDSCharacter::OnMoveForward(float Value)
{
	AxisX = Value;
}

/** Handler for MoveRight axis */
void ATDSCharacter::OnMoveRight(float Value)
{
	AxisY = Value;
}

/** Handler for CameraZoom axis */
void ATDSCharacter::OnCameraZoomChange(float Value)
{
	if (Value != 0)
	{
		const auto CurrentDelta = CameraBoom->TargetArmLength - FMath::Sign(Value) * CameraHeightDelta;
		CameraHeightInterpolate = FMath::Clamp(CurrentDelta, MinCameraHeight, MaxCameraHeight);
	}
}

/** Handler for Walk button pressed */
void ATDSCharacter::OnWalkPressed()
{
	ChangeMovementState(EMovementState::Walk_State);
}

/** Handler for Walk button released */
void ATDSCharacter::OnWalkReleased()
{
	ChangeMovementState(EMovementState::Run_State);
}

void ATDSCharacter::OnSprintPressed()
{
	ChangeMovementState(EMovementState::Sprint_State);
}

void ATDSCharacter::OnSprintReleased()
{
	ChangeMovementState(EMovementState::Run_State);
}

/** Handler for Aim button pressed */
void ATDSCharacter::OnAimPressed()
{
	ChangeMovementState(EMovementState::Aim_State);
}

/** Handler for Aim button released */
void ATDSCharacter::OnAimReleased()
{
	ChangeMovementState(EMovementState::Run_State);
}

void ATDSCharacter::OnFirePressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::OnFireReleased()
{
	AttackCharEvent(false);
}

/** Handler for test action button pressed */
void ATDSCharacter::OnTestActionPressed()
{
	//Do nothing right now
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	// Do nothing.
	return CurrentCursor;
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}
