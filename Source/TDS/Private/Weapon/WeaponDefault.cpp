// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/WeaponDefault.h"

#include "Components/ArrowComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(SceneComponent);

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
}

void AWeaponDefault::FireTick(const float DeltaTime)
{
	if (bIsWeaponFiring)
	{
		if (FireTime < 0.f)
		{
			Fire();
		}
		else
		{
			FireTime -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit() const
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire(const bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		bIsWeaponFiring = bIsFire;
	} else
	{
		bIsWeaponFiring = false;
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//TODO: Dispersion based on movement state
	ChangeDispersion();
}

void AWeaponDefault::ChangeDispersion()
{
	//TODO: Will be changed in future
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return true;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTime = WeaponSetting.GetFireRate();

	if (ShootLocation)
	{
		const auto SpawnLocation = ShootLocation->GetComponentLocation();
		const auto SpawnRotation = ShootLocation->GetComponentRotation();
		const FProjectileInfo ProjectileInfo = GetProjectile();

		if (ProjectileInfo.Projectile)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			const auto NewProjectile = GetWorld()->SpawnActor<AProjectileDefault>(ProjectileInfo.Projectile, SpawnLocation, SpawnRotation, SpawnParams);
			if (NewProjectile)
			{
				NewProjectile->InitialLifeSpan = 20.f;
			}
		}
	}
}

