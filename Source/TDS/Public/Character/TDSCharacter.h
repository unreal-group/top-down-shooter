// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon/WeaponDefault.h"
#include "Types.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(meta = (AllowPrivateAccess = "true"))
	AWeaponDefault* CurrentWeapon = nullptr;

	UFUNCTION()
	void InitWeapon();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Camera")
	float MaxCameraHeight;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Camera")
	float MinCameraHeight;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Camera")
	float CameraHeightDelta;

	UPROPERTY(BlueprintReadOnly, Category="Camera")
	float CameraHeightInterpolate;

	UPROPERTY()
	float AxisX = 0.0f;

	UPROPERTY()
	float AxisY = 0.0f;

	UPROPERTY()
	UDecalComponent* CurrentCursor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	FVector CursorSize = FVector(20.f, 40.f, 40.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Demo")
	TSubclassOf<AWeaponDefault> InitWeaponClass;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION()
	void CameraTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION()
    void ChangeMovementState(const EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
	bool IsMovingForward() const;

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	UFUNCTION()
	AWeaponDefault* GetCurrentWeapon();

	/** Input event handlers */
	
	UFUNCTION()
    void OnMoveForward(float Value);
	
	UFUNCTION()
	void OnMoveRight(float Value);

	UFUNCTION()
    void OnCameraZoomChange(float Value);

	UFUNCTION()
	void OnWalkPressed();

	UFUNCTION()
	void OnWalkReleased();

	UFUNCTION()
	void OnSprintPressed();

	UFUNCTION()
	void OnSprintReleased();

	UFUNCTION()
	void OnAimPressed();

	UFUNCTION()
	void OnAimReleased();

	UFUNCTION()
	void OnFirePressed();

	UFUNCTION()
	void OnFireReleased();

	UFUNCTION()
	void OnTestActionPressed();
};

