// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Types.h"
#include "GameFramework/Actor.h"
#include "Weapon/Projectile/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USceneComponent* SceneComponent = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* StaticMeshWeapon = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FWeaponInfo WeaponSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool bIsWeaponFiring = false;

	float FireTime = 0.f;

	void WeaponInit() const;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	
	UFUNCTION()
	void Fire();

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);

	void UpdateStateWeapon(EMovementState NewMovementState);

	void ChangeDispersion();

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();

};
